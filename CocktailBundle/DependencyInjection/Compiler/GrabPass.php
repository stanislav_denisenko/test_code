<?php

namespace Api\CocktailBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class GrabPass
 *
 * @package Api\CocktailBundle\DependencyInjection\Compiler
 */
class GrabPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('grab_sites') || !$container->getParameter('grab_sites')) {
            return;
        }

        $className = 'Api\\CocktailBundle\\Service\\Grabbers\\%sGrab';
        foreach ($container->getParameter('grab_sites') as $name => $options) {

            $class = sprintf($className, ucfirst($name));
            if(!class_exists($class)) {
                continue;
            }

            $definition = new Definition($class);
            $definition->setArguments([$options]);

            $container->setDefinition(sprintf('api.%s._grab', $name), $definition);
        }
    }
}