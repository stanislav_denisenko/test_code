<?php

namespace Api\CocktailBundle\Service\Grabbers;

use Api\CocktailBundle\Service\GrabAbstract;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class BarclassGrab
 *
 * @package Api\CocktailBundle\Service\Grabbers
 */
class BarclassGrab extends GrabAbstract
{

    /**
     * Grab cocktail info page
     *
     * @param string $url
     *
     * @return array
     */
    public function getCocktailsInformation($url)
    {
        if (!$url) {
            return [];
        }

        $dom = $this->getPage($url);

        // Parse item page for information
        $cocktail[] = [
            'cocktailUrl' => 'url',
            'image' => 'image',
            'name' => 'name',
            'discription' => 'description',
        ];

        return $cocktail;
    }

    /**
     * Parse page for links
     *
     * @return array
     * @throws \DOMException
     */
    public function getCocktails()
    {
        $cocktails = [];
        try {
            $page = $this->getPage($this->cocktailsUrl);
            $links = $page->filter('selector to get href links');

            /** @var Crawler $link */
            foreach ($links as $link) {
                $cocktails[] = $this->getCocktailsInformation($this->siteUrl . $link->attr('href'));
            }
        } catch (\DOMException $e) {
            throw new \DOMException('Failed parse cocktail site: ' . $this->siteUrl, 500);
        }


        return $cocktails;
    }
}