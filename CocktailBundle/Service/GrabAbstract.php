<?php

namespace Api\CocktailBundle\Service;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GrabAbstract
 *
 * @package Api\CocktailBundle\Service
 */
abstract class GrabAbstract
{
    /**
     * @var string
     */
    protected $siteUrl;

    /**
     * @var string
     */
    protected $cocktailsUrl;

    /**
     * @var array
     */
    protected $options;

    /**
     * GrabAbstract constructor.
     * @param array $options
     */
    public function __construct($options)
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['cocktails_url', 'site_url']);
    }

    /**
     * @param string $url
     *
     * @return Crawler
     */
    protected function getPage($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);

        return new Crawler($html);
    }

    /**
     * Grab cocktail info page
     *
     * @param string $url
     *
     * @return array
     */
    abstract public function getCocktailsInformation($url);

    /**
     * Parse page for links
     *
     * @throws \DOMException
     */
    abstract public function getCocktails();
}