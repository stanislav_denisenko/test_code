<?php

namespace Api\CocktailBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GrabFactory
 *
 * @package Api\CocktailBundle\Service
 */
class GrabFactory
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $grabName
     *
     * @return object
     */
    public function get($grabName)
    {
        return $this->container->get(sprintf('api.%s._grab', $grabName));
    }
}