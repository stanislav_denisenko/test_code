<?php

namespace Api\CocktailBundle;

use Api\CocktailBundle\DependencyInjection\Compiler\GrabPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ApiCocktailBundle
 *
 * @package Api\CocktailBundle
 */
class ApiCocktailBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new GrabPass());
    }
}
